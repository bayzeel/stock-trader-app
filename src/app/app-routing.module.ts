import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MarketsComponent } from './components/markets/markets.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';

const appRoutes: Routes = [
  { path: 'markets', component: MarketsComponent },
  { path: 'portfolio', component: PortfolioComponent },
  { path: '',   redirectTo: '/markets', pathMatch: 'full' },
  { path: '**',   redirectTo: '/markets', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot( appRoutes )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
