import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { StockModel } from './models/stock.model';

import { GetAllStock } from './store/stocks.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor (
    private readonly storeGetMainStockList: Store<{mainStockList: Array<StockModel>}>
  ) { }

  ngOnInit () {
    this.storeGetMainStockList.dispatch( new GetAllStock() );
  }
}
