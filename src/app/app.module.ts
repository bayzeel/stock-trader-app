import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppMaterialModule } from './app-material.module';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import {
  mainStocksReducer,
  portfolioStocksReducer
} from './store/stocks.reducers';
import { buyingPowerReducer } from './store/buying-power.reducers';

import {
  MainStocksEffects,
  PortfolioStocksEffects
} from './effects/stocks.effects';

import { AppComponent } from './app.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { HeaderComponent } from './components/header/header.component';
import { MarketsComponent } from './components/markets/markets.component';
import { NoticeBarComponent } from './components/notice-bar/notice-bar.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';

import { DialogService } from './services/dialog.service';
import { NoticeService } from './services/notice.service';
import { StocksDataService } from './services/stocks-data.service';

@NgModule({
  entryComponents: [
    DialogComponent,
    NoticeBarComponent
  ],
  declarations: [
    AppComponent,
    DialogComponent,
    HeaderComponent,
    NoticeBarComponent,
    MarketsComponent,
    PortfolioComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AppMaterialModule,
    StoreModule.forRoot( {
      'mainStockList': mainStocksReducer,
      'portfolioStockList': portfolioStocksReducer,
      'buyingPower': buyingPowerReducer
    } ),
    EffectsModule.forRoot( [
      MainStocksEffects,
      PortfolioStocksEffects
    ] )
  ],
  providers: [
    DialogService,
    NoticeService,
    StocksDataService
  ],
  exports: [HeaderComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
