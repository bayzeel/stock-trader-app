import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

import { Store } from '@ngrx/store';

import { StockModel } from '../../models/stock.model';
import { BuyStock, PortfolioStockActionsNS, SellStock, UpdateStock } from '../../store/stocks.actions';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  public quantity = 1;
  public buyingPower = 0;
  private portfolioStockListOrigin: Array<{stock: StockModel, quantity: number}>;

  constructor (
    @Inject( MAT_DIALOG_DATA ) public data: any,
    private readonly storeBuyingPower: Store<{buyingPower: number}>,
    private readonly storePortfolioStockList: Store<{portfolioStockList: Array<{stock: StockModel, quantity: number}>}>
  ) { }

  ngOnInit () {
    this.storeBuyingPower.select( 'buyingPower' ).subscribe( storeObj => {
      this.buyingPower = storeObj['buyingPower'];
    } );

    this.storePortfolioStockList.select( 'portfolioStockList' ).subscribe( portfolioStockListResponse => {
      this.portfolioStockListOrigin = [...portfolioStockListResponse];
    } );

    if ( this.data.type === PortfolioStockActionsNS.SELL ) {
      this.quantity = this.data.quantity;
    }
  }

  ckeckInput () {
    if ( this.quantity < 1 ) {
      this.quantity = 1;
    }
  }

  buyStock () {
    const portfolioStockListSize = this.portfolioStockListOrigin.length;
    let i = 0;

    for ( ; i < portfolioStockListSize; i++ ) {
      if ( this.portfolioStockListOrigin[i].stock.id === this.data.stock.id ) {
        return this.storePortfolioStockList.dispatch(
          new UpdateStock( {
            stock: this.data.stock,
            previousQuantity: this.portfolioStockListOrigin[i].quantity,
            currentQuantity: ( this.portfolioStockListOrigin[i].quantity + this.quantity )
          } )
        );
      }
    }

    return this.storePortfolioStockList.dispatch( new BuyStock({stock: this.data.stock, quantity: this.quantity} ) );
  }

  sellStock () {
    if ( this.data.quantity === this.quantity ) {
      return this.storePortfolioStockList.dispatch( new SellStock({stock: this.data.stock, quantity: this.quantity} ) );
    }

    return this.storePortfolioStockList.dispatch(
      new UpdateStock({
        stock: this.data.stock,
        previousQuantity: this.data.quantity,
        currentQuantity: ( this.data.quantity - this.quantity )
      } )
    );
  }
}
