import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public buyingPower = 0;

  constructor (
    private readonly storeBuyingPower: Store<{buyingPower: number}>
  ) { }

  ngOnInit () {
    this.storeBuyingPower.select( 'buyingPower' ).subscribe( storeObj => {
      this.buyingPower = storeObj['buyingPower'];
    } );
  }
}
