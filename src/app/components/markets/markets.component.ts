import { Component, OnInit, ViewChild } from '@angular/core';

import { Store } from '@ngrx/store';

import { MainStockActionsNS, PortfolioStockActionsNS } from '../../store/stocks.actions';

import { StockModel } from '../../models/stock.model';

import { MatSort, MatTableDataSource } from '@angular/material';

import { DialogService } from '../../services/dialog.service';

@Component({
  selector: 'app-markets',
  templateUrl: './markets.component.html',
  styleUrls: ['./markets.component.scss']
})
export class MarketsComponent implements OnInit {
  public mainStockList: MatTableDataSource<StockModel>;

  @ViewChild( MatSort ) sort: MatSort;

  constructor (
    private readonly storeMainStockList: Store<{mainStockList: ( Array<StockModel> | string )}>,
    public readonly dialogService: DialogService
  ) { }

  ngOnInit () {
    this.storeMainStockList.select( 'mainStockList' ).subscribe( mainStockListResponse => {
      if ( mainStockListResponse !== MainStockActionsNS.GET_ALL_ERROR ) {
        this.mainStockList = new MatTableDataSource( mainStockListResponse as Array<StockModel> );
      }

      this.mainStockList.sort = this.sort;
    } );
  }

  putStockDataToDialog ( stock ) {
    const dialogDataObj = {
      type: PortfolioStockActionsNS.BUY,
      stock: stock
    };

    this.dialogService.open( dialogDataObj );
  }

  applyFilter( filterValue: string ) {
    this.mainStockList.filter = filterValue.trim().toLowerCase();
  }
}
