import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatSnackBarModule, MAT_SNACK_BAR_DATA } from '@angular/material';

import { NoticeBarComponent } from './notice-bar.component';

describe('NoticeBarComponent', () => {
  let component: NoticeBarComponent;
  let fixture: ComponentFixture<NoticeBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NoticeBarComponent
      ],
      imports: [
        MatSnackBarModule
      ],
      providers: [
        {
          provide: MAT_SNACK_BAR_DATA,
          useValue: {}
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticeBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
