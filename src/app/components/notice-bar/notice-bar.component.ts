import { Component, Inject, OnInit } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-notice-bar',
  templateUrl: './notice-bar.component.html',
  styleUrls: ['./notice-bar.component.scss']
})
export class NoticeBarComponent implements OnInit {

  constructor (
    @Inject( MAT_SNACK_BAR_DATA ) public data: any,
    public snackBar: MatSnackBar
  ) { }

  dismiss () {
    this.snackBar.dismiss();
  }

  ngOnInit () { }
}
