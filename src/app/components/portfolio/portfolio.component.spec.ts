import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { StoreModule } from '@ngrx/store';

import { MatTableModule } from '@angular/material';

import { PortfolioComponent } from './portfolio.component';

import { DialogService } from '../../services/dialog.service';

describe('PortfolioComponent', () => {
  let component: PortfolioComponent,
    fixture: ComponentFixture<PortfolioComponent>,
    mockDialogStub;

  beforeEach(async(() => {
    mockDialogStub = {
      open: () => {}
    };

    TestBed.configureTestingModule({
      declarations: [
        PortfolioComponent
      ],
      imports: [
        StoreModule.forRoot( {} ),
        NoopAnimationsModule,
        MatTableModule
      ],
      providers: [
        {
          provide: DialogService,
          useValue: mockDialogStub
        }
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#putStockDataToDialog should open the dialog', () => {
    const dummyStock = {id: 1, name: 'MaxPoint Interactive, Inc.', price: 298.90, category: 'Consumer Services'};

    component.putStockDataToDialog(dummyStock);

    expect(component.putStockDataToDialog.length).toEqual(1);
  });

  it('#totalSumm should calculate a summ', () => {
    const dummyStocks = [
      {
        stock: {id: 1, name: 'MaxPoint Interactive, Inc.', price: 125, category: 'Consumer Services'},
        quantity: 2
      },
      {
        stock: {id: 2, name: 'Jensyn Acquistion Corp.', price: 175, category: 'Finance'},
        quantity: 2
      }
    ];

    component.portfolioStockListOrigin = [...dummyStocks];

    expect(component.totalSumm()).toEqual(600);
  });
});
