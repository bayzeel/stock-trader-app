import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { PortfolioStockActionsNS } from '../../store/stocks.actions';

import { StockModel } from '../../models/stock.model';

import { MatTableDataSource } from '@angular/material';

import { DialogService } from '../../services/dialog.service';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {
  public portfolioStockListOrigin: Array<{stock: StockModel, quantity: number}> = [] as Array<{stock: StockModel, quantity: number}>;
  public portfolioStockList: MatTableDataSource<{stock: StockModel, quantity: number}>;

  constructor (
    private readonly storePortfolioStockList: Store<{portfolioStockList: Array<{stock: StockModel, quantity: number}>}>,
    public readonly dialogService: DialogService
  ) { }

  ngOnInit () {
    this.storePortfolioStockList.select( 'portfolioStockList' ).subscribe( portfolioStockListResponse => {
      this.portfolioStockListOrigin = [...portfolioStockListResponse];
      this.portfolioStockList = new MatTableDataSource( [...portfolioStockListResponse] );
    } );
  }

  putStockDataToDialog ( stock ) {
    const dialogDataObj = {
      type: PortfolioStockActionsNS.SELL,
      stock: stock.stock,
      quantity: stock.quantity
    };

    this.dialogService.open( dialogDataObj );
  }

  totalSumm () {
    let summ = 0;

    this.portfolioStockListOrigin.forEach( item => {
      summ = summ + +( item.stock.price * item.quantity ).toFixed( 2 );
      summ = +summ.toFixed( 2 );
    } );

    return summ;
  }
}
