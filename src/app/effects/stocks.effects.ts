import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';

import { Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';

import {
  GetAllStockError,
  GetAllStockSuccess,
  MainStockActionsNS,
  PortfolioStockActionsNS
} from '../store/stocks.actions';

import { StocksDataService } from '../services/stocks-data.service';

import { StockModel } from '../models/stock.model';
import { DecreaseBuyingPower, IncreaseBuyingPower } from '../store/buying-power.actions';
import { NoticeService } from '../services/notice.service';

@Injectable()
export class MainStocksEffects {
  @Effect( {dispatch: false} )
  getAllStocks$ = this.actions$.pipe(
    ofType( MainStockActionsNS.GET_ALL ),
    tap( () => {
      this.stocksDataService.getData().subscribe( allStocks => {
        return this.storeMainStockList.dispatch( new GetAllStockSuccess( allStocks ) );
      }, err => {
        console.error( err );
        return this.storeMainStockList.dispatch( new GetAllStockError() );
      } );
    } )
  );

  constructor (
    private readonly actions$: Actions,
    private readonly storeMainStockList: Store<{mainStockList: Array<StockModel>}>,
    private readonly stocksDataService: StocksDataService,
  ) { }
}

@Injectable()
export class PortfolioStocksEffects {
  @Effect( {dispatch: false} )
  buyStock$ = this.actions$.pipe(
    ofType( PortfolioStockActionsNS.BUY ),
    map( actionData => {
      const decBP = +( actionData['payload'].stock.price * actionData['payload'].quantity ).toFixed( 2 );
      this.noticeService.openNoticeBar( 'Stock bought successfully!');
      return this.storeBuyingPower.dispatch( new DecreaseBuyingPower( decBP ) );
    } )
  );

  @Effect( {dispatch: false} )
  sellStock$ = this.actions$.pipe(
    ofType( PortfolioStockActionsNS.SELL ),
    map( actionData => {
      const incBP = +( actionData['payload'].stock.price * actionData['payload'].quantity ).toFixed( 2 );
      this.noticeService.openNoticeBar( 'Stock sold successfully!');
      return this.storeBuyingPower.dispatch( new IncreaseBuyingPower( incBP ) );
    } )
  );

  @Effect( {dispatch: false} )
  updateStock$ = this.actions$.pipe(
    ofType( PortfolioStockActionsNS.UPDATE ),
    map( actionData => {
      const prev = +( actionData['payload'].stock.price * actionData['payload'].previousQuantity ).toFixed( 2 );
      const curr = +( actionData['payload'].stock.price * actionData['payload'].currentQuantity ).toFixed( 2 );

      if ( actionData['payload'].currentQuantity < actionData['payload'].previousQuantity ) {
        const incBP = prev - curr;
        this.noticeService.openNoticeBar( 'Stock sold successfully!');
        return this.storeBuyingPower.dispatch( new IncreaseBuyingPower( incBP ) );
      }

      const decBP = curr - prev;
      this.noticeService.openNoticeBar( 'Stock bought successfully!');
      return this.storeBuyingPower.dispatch( new DecreaseBuyingPower( decBP ) );
    } )
  );

  constructor (
    private readonly actions$: Actions,
    private readonly storeBuyingPower: Store<{storeBuyingPower: number}>,
    private readonly noticeService: NoticeService,
  ) { }
}
