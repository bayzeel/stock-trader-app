import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { TestBed, inject, getTestBed } from '@angular/core/testing';

import { MatDialogModule, MatInputModule } from '@angular/material';

import { DialogComponent } from '../components/dialog/dialog.component';

import { DialogService } from './dialog.service';

describe('DialogService', () => {
  let injector: TestBed,
    spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DialogComponent
      ],
      imports: [
        NoopAnimationsModule,
        FormsModule,
        MatDialogModule,
        MatInputModule
      ],
      providers: [
        {
          provide: DialogService,
          useValue: jasmine.createSpyObj('DialogService', ['open'])
        }
      ]
    });
    injector = getTestBed();
    spy = jasmine.createSpyObj('DialogService', ['open']);
  });

  it('should be created', () => {
    expect(spy).toBeTruthy();
  });

  it('should open dialog', () => {
    spy.open({data: 'Fake data'});
    setTimeout( () => {
      expect(spy.open.calls.count()).toEqual(1);
      expect(spy.open.first().args).toEqual([{data: 'Fake data'}]);
    }, 200 );
  });
});
