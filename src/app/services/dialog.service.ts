import { Injectable } from '@angular/core';

import { MatDialog } from '@angular/material';

import { DialogComponent } from '../components/dialog/dialog.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(
    public readonly dialog: MatDialog
  ) { }

  open ( dataObj: object ) {
    this.dialog.open( DialogComponent, {
      width: '300px',
      data: {...dataObj}
    } );
  }
}
