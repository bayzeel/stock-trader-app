import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TestBed, getTestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';

import { MatSnackBarModule } from '@angular/material';

import { NoticeBarComponent } from '../components/notice-bar/notice-bar.component';

import { NoticeService } from './notice.service';

import { Action, StoreModule } from '@ngrx/store';
import { provideMockActions } from '@ngrx/effects/testing';

import { portfolioStocksReducer } from '../store/stocks.reducers';
import { PortfolioStocksEffects } from '../effects/stocks.effects';

describe('NoticeService', () => {
  let injector: TestBed,
    spy,
    actions$: Observable<Action>,
    effects: PortfolioStocksEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        NoticeBarComponent
      ],
      providers: [
        PortfolioStocksEffects,
        provideMockActions( () => actions$ ),
        {
          provide: NoticeService,
          useValue: jasmine.createSpyObj('NoticeService', ['openNoticeBar'])
        }
      ],
      imports: [
        StoreModule.forRoot( {
          'portfolioStockList': portfolioStocksReducer
        } ),
        NoopAnimationsModule,
        MatSnackBarModule
      ]
    });
    injector = getTestBed();
    spy = jasmine.createSpyObj('NoticeService', ['openNoticeBar']);
    effects = TestBed.get(PortfolioStocksEffects);
  });

  it('should be created', () => {
    expect(spy).toBeTruthy();
  });

  it('should open notice bar', () => {
    effects.buyStock$.subscribe( () => {
      spy.openNoticeBar('Stock bought successfully!');
    });
    setTimeout(() => {
      expect(spy.openNoticeBar.calls.count()).toEqual(1);
      expect(spy.openNoticeBar.first().args).toEqual(['Stock bought successfully!']);
    }, 200 );
  });
});
