import { Injectable } from '@angular/core';

import { MatSnackBar, MatSnackBarHorizontalPosition } from '@angular/material';

import { NoticeBarComponent } from '../components/notice-bar/notice-bar.component';

@Injectable({
  providedIn: 'root'
})
export class NoticeService {

  constructor (
    public snackBar: MatSnackBar
  ) { }

  openNoticeBar (
    message: string,
    isSucceed: boolean = true,
    horizontalPosition: MatSnackBarHorizontalPosition = 'start',
    panelClass: string = '',
    duration: number = 0
  ) {
    if ( !isSucceed ) {
      if ( !duration ) {
        duration = 5000;
      }
      if ( !panelClass ) {
        panelClass = 'mat-snack-bar-container__error';
      }
    } else {
      if ( !duration ) {
        duration = 2500;
      }
    }

    this.snackBar.openFromComponent( NoticeBarComponent, {
      data: { message, isSucceed },
      horizontalPosition,
      panelClass,
      duration
    } );
  }
}
