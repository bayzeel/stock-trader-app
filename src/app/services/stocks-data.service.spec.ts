import { TestBed, getTestBed, inject } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { StocksDataService } from './stocks-data.service';

describe('StocksDataService', () => {
  let injector: TestBed;
  let service: StocksDataService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        StocksDataService
      ],
      imports: [
        HttpClientTestingModule
      ]
    });
    injector = getTestBed();
    service = injector.get(StocksDataService);
    httpMock = injector.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return an Observable<Array<StockModel>>', () => {
    const dummyStocks = [
      {id: 1, name: 'MaxPoint Interactive, Inc.', price: 298.90, category: 'Consumer Services'},
      {id: 2, name: 'Jensyn Acquistion Corp.', price: 287.63, category: 'Finance'}
    ];

    service.getData().subscribe(stocks => {
      expect(stocks.length).toBe(2);
      expect(stocks).toEqual(dummyStocks);
    });

    const req = httpMock.expectOne('./assets/db/stocks.json');
    expect(req.request.method).toBe('GET');
    req.flush(dummyStocks);
  });
});
