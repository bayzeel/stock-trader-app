import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { StockModel } from '../models/stock.model';

@Injectable({
  providedIn: 'root'
})
export class StocksDataService {

  constructor (
    private readonly http: HttpClient
  ) { }

  getData (): Observable<Array<StockModel>> {
    return this.http.get<Array<StockModel>>( './assets/db/stocks.json' );
  }
}
