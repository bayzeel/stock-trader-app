import { Action } from '@ngrx/store';

export namespace BuyingPowerActionsNS {
  export const INCREASE_BP = 'INCREASE_BP';
  export const DECREASE_BP = 'DECREASE_BP';
}

export class IncreaseBuyingPower implements Action {
  readonly type = BuyingPowerActionsNS.INCREASE_BP;
  constructor ( public payload: number ) { }
}

export class DecreaseBuyingPower implements Action {
  readonly type = BuyingPowerActionsNS.DECREASE_BP;
  constructor ( public payload: number ) { }
}

export type BuyingPowerActions =
  IncreaseBuyingPower |
  DecreaseBuyingPower;
