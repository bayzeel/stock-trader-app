import { config } from '../config';

import { BuyingPowerActions, BuyingPowerActionsNS } from './buying-power.actions';

const initialState = {
  buyingPower: config.buyingPower
};

export function buyingPowerReducer ( state = initialState, action: BuyingPowerActions ) {
  switch ( action.type ) {
    case BuyingPowerActionsNS.INCREASE_BP:
      return {
        ...state,
        buyingPower: state.buyingPower = +( state.buyingPower + action.payload ).toFixed( 2 )
      };
    case BuyingPowerActionsNS.DECREASE_BP:
      return {
        ...state,
        buyingPower: state.buyingPower -= action.payload
      };
    default:
      return state;
  }
}
