import { Action } from '@ngrx/store';

import { StockModel } from '../models/stock.model';

export namespace MainStockActionsNS {
  export const GET_ALL = 'GET_ALL';
  export const GET_ALL_SUCCESS = 'GET_ALL_SUCCESS';
  export const GET_ALL_ERROR = 'GET_ALL_ERROR';
}

export class GetAllStock implements Action {
  readonly type = MainStockActionsNS.GET_ALL;
}

export class GetAllStockSuccess implements Action {
  readonly type = MainStockActionsNS.GET_ALL_SUCCESS;
  constructor ( public payload: Array<StockModel> ) { }
}

export class GetAllStockError implements Action {
  readonly type = MainStockActionsNS.GET_ALL_ERROR;
}

export type MainStockActions =
  GetAllStock |
  GetAllStockSuccess |
  GetAllStockError;


export namespace PortfolioStockActionsNS {
  export const BUY = 'BUY';
  export const SELL = 'SELL';
  export const UPDATE = 'UPDATE';
}

export class BuyStock implements Action {
  readonly type = PortfolioStockActionsNS.BUY;
  constructor ( public payload: {stock: StockModel, quantity: number} ) { }
}

export class SellStock implements Action {
  readonly type = PortfolioStockActionsNS.SELL;
  constructor ( public payload: {stock: StockModel, quantity: number} ) { }
}

export class UpdateStock implements Action {
  readonly type = PortfolioStockActionsNS.UPDATE;
  constructor ( public payload: {stock: StockModel, previousQuantity: number, currentQuantity: number} ) { }
}

export type PortfolioStockActions =
  BuyStock |
  SellStock |
  UpdateStock;
