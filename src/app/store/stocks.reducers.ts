import {
  MainStockActions,
  MainStockActionsNS,
  PortfolioStockActions,
  PortfolioStockActionsNS
} from './stocks.actions';

const initialState = {
  mainStockList: [],
  portfolioStockList: []
};

export function mainStocksReducer ( state = initialState.mainStockList, action: MainStockActions ) {
  switch ( action.type ) {
    case MainStockActionsNS.GET_ALL:
      return [
        ...state
      ];
    case MainStockActionsNS.GET_ALL_SUCCESS:
      return [
        ...state,
        ...action.payload
      ];
    case MainStockActionsNS.GET_ALL_ERROR:
      return MainStockActionsNS.GET_ALL_ERROR;
    default:
      return state;
  }
}

export function portfolioStocksReducer ( state = initialState.portfolioStockList, action: PortfolioStockActions ) {
  let newState = [];

  switch ( action.type ) {
    case PortfolioStockActionsNS.BUY:
      return [
        ...state,
        action.payload
      ];
    case PortfolioStockActionsNS.SELL:
      newState = [...state];
      return newState.filter( item => item.stock.id !== action.payload.stock.id );
    case PortfolioStockActionsNS.UPDATE:
      newState = [...state];

      newState.forEach( ( item, idx ) => {
        if ( item.stock.id === action.payload.stock.id ) {
          newState[idx].quantity = action.payload.currentQuantity;
        }
      } );

      return newState;
    default:
      return state;
  }
}
